/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleShips;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JLabel;

/**
 *
 * @author Luke
 */
public class gridReference extends JLabel{
   private boolean hit = false;
   private boolean occupied = false;
   private boolean enemyMode = false;
   private int conflicts = 0;
   private int weight = 0;
   private boolean clicked = false;
   private Dimension gridLocation = new Dimension(0,0);
   private boolean forceMiss = false;
   
    public gridReference(){
        super();
        setSuperDefaults();
        setColors();
    }
    private void setSuperDefaults(){
        setSize(1,1);
        setOpaque(true);
        setVisible(true);
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                clickHandler(evt);
            }
        });
    }
    private void clickHandler(java.awt.event.MouseEvent e){
        if(enemyMode == true){
            clicked = true;
        }
        setColors();
    }
    
    
    
    
    
    
    public void setColors(){
        Color shotShip = Color.ORANGE;
        Color unshotShip = new Color(180,180,180);
        Color shotWater = new Color(120,180,255);
        Color unshotWater = new Color(70,120,255);
        setText("");
        if(enemyMode == false){
            // Display Normal Colors
            if (occupied == false){
                if(hit == false){
                    setBackground(unshotWater);
                }else{
                    setBackground(shotWater);
                    
                }
            }else{
                
                if(hit == false){
                    setBackground(unshotShip);
                }else{
                    setBackground(shotShip);
                }
            }
        }else{
            // Hide occupied squares
            setBackground(unshotWater);
            if (hit == false){
                if(occupied == false){
                    setBackground(unshotWater);
                }else{
                    setBackground(unshotWater);
                }
            }else{
                if(occupied == false){
                    setBackground(shotWater);
                }else{
                    setBackground(shotShip);
                }
            }
        }
        if(clicked == true && enemyMode == true){
            setBackground(Color.GREEN);
        }
        if(conflicts > 0){
            setBackground(new Color(255,0,0));
        }
    }
    
    public int getConflicts(){
        return conflicts;
    }
    public boolean getMiss(){
        if(forceMiss == true){
            return true;
        }else if(occupied == false && hit == true){
            return true;
        }else{
            return false;
        }
    }
    public boolean getClicked(){
        return clicked;
    }
    public boolean getHit(){
        return hit;
    }
    public Dimension getGridLocation() {
        return gridLocation;
    }
    public int getWeight(){
        return weight;
    }
    public boolean getOccupied(){
        return occupied;
    }
    public void setClicked(boolean newClicked){
        clicked = newClicked;
        setColors();
    }
    public void setGridLocation(Dimension newGridLocation){
        gridLocation = newGridLocation;
    }
    public void setEnemyMode(boolean newEnemyMode){ 
        enemyMode = newEnemyMode;
    }
    public void setHit(boolean newHit){
        hit = newHit;
    }
    public void setOccupied(boolean newOccupied){
        occupied = newOccupied;
    }
    public void setForceMiss(boolean newForceMiss) {
       forceMiss = newForceMiss; 
    }
    public void addWeight(int addWeight) {
        if (hit == false){
            weight+=addWeight;
        }else{
            weight = 0;
        }
    }
    public void modConflicts(int addConflict){
        conflicts += addConflict; 
    }
    public void resetWeight() {
       weight = 0;
    }

    
    
}
