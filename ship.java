/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleShips;

import java.awt.Dimension;
 
/**
 *
 * @author Luke
 */
public class ship {
    private gridReference[] container;
    private int length;
    private Dimension startPos;
    private Dimension endPos;
    private String name;
    private static int numberOfShips;
     
    public ship(gridReference[] newContainer){
        container = newContainer;
        setContainer(newContainer);
        length = container.length;
        startPos = container[0].getGridLocation();
        endPos = container[length-1].getGridLocation();
        numberOfShips++;
    }
    public void setName(String newName){
        name = newName;
    }
    public void setContainer(gridReference[] newContainer){
        // Loop to unoccupy previous container.
        for(int x = 0; x< length; x++){
           if(container[x].getConflicts()==0){
                container[x].setOccupied(false);
           }
        } 
       
       length = newContainer.length;
       // Loop to occupy newContainer
       for(int x = 0; x< length; x++){
           container[x] = newContainer[x];
           container[x].setOccupied(true);
       }
       
       // Correctly alter new start and end positions.
       startPos = newContainer[0].getGridLocation();
       endPos = newContainer[length-1].getGridLocation();
    }
    public String getName(){
       return name;
    }
    public Dimension getStartPos(){
        return startPos;
    }
    public boolean getSunk(){
        for(int shipSlot = 0; shipSlot<length;shipSlot++){
            if(container[shipSlot].getHit() == false){
                return false;
            }
        }
        return true;
    }
    public int getOrientation(){
        if(startPos.width == endPos.width){
            return 1;
        }else 
            return 0;
    }
    public gridReference[] getContainer(){
        return container;
    }
    public int getLength(){
        return length;
    }
    public boolean hasConflicts(){
        for(int shipSlot = 0; shipSlot<(length-1);shipSlot++){
            if(container[shipSlot].getConflicts()>0){
                return true;
            }
        }
        return false;
    }
    
 
    public boolean doesContain(gridReference testGridReference){
        boolean tempContainer = false;
        for(int x = 0; x<length; x++){
            if (container[x] == testGridReference){
                tempContainer = true;
                break;
            }
        }
        return tempContainer;
    }
    
    
}
