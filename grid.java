/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleShips;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.*;

/**
 *
 * @author Luke
 */
public class grid extends JPanel {

    private gridReference[][] grdRefs = new gridReference[10][10];
    private ship[] ships;
    private ArrayList<ship> sunkShips = new ArrayList<ship>();
    private boolean aiMode = false;
    private ArrayList<gridReference> focusArea = new ArrayList<gridReference>();
    private int difficulty = 0;

    public grid() {
        super();
        setSuperDefaults();
        addComponents();
        initShips();
        setGridColors();
    }

    private void setSuperDefaults() {
        try {
            setLayout(new GridLayout(10, 10, 1, 1));
        } catch (NullPointerException e) {
            System.err.println("SetSuperDefaults Err");
        }
    }

    private void addComponents() {
        try {
            for (int x = 9; x > -1; x--) {
                for (int y = 0; y < 10; y++) {
                    // Loop through initialising all the gridReferences
                    grdRefs[y][x] = new gridReference();
                    // Set up gridLocation
                    grdRefs[y][x].setGridLocation(new Dimension(y, x));
                    // Add click handler
                    grdRefs[y][x].addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                            clickHandler();
                        }
                    });
                    add(grdRefs[y][x]);
                }
            }
        } catch (NullPointerException e) {
            System.err.print("AddComponents Err");
        }
    }

    public ArrayList<ship> getSunkShips() {
        return sunkShips;
    }

    private void clickHandler() {
        // Triggered when any gridReference is clicked
        ArrayList<gridReference> refsClicked = new ArrayList();
        for (int x = 0; x < 10; x++) {
            for (int y = 0; y < 10; y++) {
                if (grdRefs[x][y].getClicked() == true) {
                    refsClicked.add(grdRefs[x][y]);
                }
            }
        }
        if (refsClicked.size() > 1) {
            // If more than one is clicked, 'unclicks' all of them.
            for (int x = 0; x < refsClicked.size(); x++) {
                refsClicked.get(x).setClicked(false);
            }
        }
    }

    public boolean hasClicked() {
        // Checks to see if any gridReferences in the array have been clicked.
        for (int x = 0; x < 10; x++) {
            for (int y = 0; y < 10; y++) {
                if (grdRefs[x][y].getClicked() == true) {
                    return true;
                }
            }
        }
        return false;
    }

    public void resetClicked() {
        for (int x = 0; x < 10; x++) {
            for (int y = 0; y < 10; y++) {
                grdRefs[x][y].setClicked(false);
            }
        }
    }

    public void setAiMode(boolean newAiMode) {
        aiMode = newAiMode;
    }

    public void fire() {
        if (aiMode == false) {
            // Fires at clicked reference
            outerLoop:
            for (int x = 0; x < 10; x++) {
                for (int y = 0; y < 10; y++) {
                    if (grdRefs[x][y].getClicked() == true) {
                        grdRefs[x][y].setClicked(false);
                        grdRefs[x][y].setHit(true);
                        break outerLoop;
                    }
                }
            }
            handleSunkShips();
        } else {
            // Fires at algorithms result
            resetGridWeight();
            for (int lineRef = 0; lineRef < 10; lineRef++) {
                weightCalc(grdRefs[lineRef]);
                gridReference[] tempVerticalLine = new gridReference[10];
                for (int x = 0; x < 10; x++) {
                    tempVerticalLine[x] = grdRefs[x][lineRef];
                }
                weightCalc(tempVerticalLine);
            }
            gridReference targetToHit;
            if (difficulty == 1) {
                targetToHit = findTargets()[2];
            } else if (difficulty == 2) {
                targetToHit = findTargets()[1];
            } else {
                targetToHit = findTargets()[0];
            }
            System.out.println("" + targetToHit.getGridLocation().width + targetToHit.getGridLocation().height);
            targetToHit.setHit(true);
            try {
                if (targetToHit.getMiss() == false) {
                    int tempSunkShips = sunkShips.size();
                    handleSunkShips();
                    if (tempSunkShips == sunkShips.size()) {
                        focusArea.add(targetToHit);
                    } else {
                        for (int x = 0; x < focusArea.size(); x++) {
                            focusArea.get(x).setForceMiss(true);
                        }
                        focusArea.clear();
                    }
                }
            } catch (NullPointerException e) {
                System.err.println("Error setting new");
            }
        }
        setGridColors();
    }

    private void handleSunkShips() {
        sunkShips.clear();
        for (int x = 0; x < 5; x++) {
            if (ships[x].getSunk() == true) {
                sunkShips.add(ships[x]);
            }
        }
    }

    public void randomlyPlaceShips() {
        for (int x = 0; x < 5; x++) {
            placeShip(x, new Dimension(5, x), 0);
        }
        Random rand = new Random();
        //rand.nextInt(max - min + 1) + min
        for (int shipID = 0; shipID < 5; shipID++) {
            Dimension randStartPos;
            int randOrientation = rand.nextInt(2);
            try {
                do {
                    if (randOrientation == 0) {
                        int widthMax = 9 - ships[shipID].getLength();
                        int widthMin = 0;
                        int heightMax = 9;
                        int heightMin = 0;
                        randStartPos = new Dimension(rand.nextInt(widthMax - widthMin + 1) + widthMin, rand.nextInt(heightMax - heightMin + 1) + heightMin);
                    } else {
                        int widthMax = 9;
                        int widthMin = 0;
                        int heightMax = 9;
                        int heightMin = ships[shipID].getLength();
                        randStartPos = new Dimension(rand.nextInt(widthMax - widthMin + 1) + widthMin, rand.nextInt(heightMax - heightMin + 1) + heightMin);
                    }
                    placeShip(shipID, randStartPos, randOrientation);
                } while (ships[shipID].hasConflicts() == true);
            } catch (NullPointerException e) {
                System.err.println("NullPointer in random ship");
            }
        }
        if (gridHasConflicts() == true) {
            randomlyPlaceShips();
        }
        setGridColors();
    }

    private gridReference[] findTargets() {
        gridReference[] tempReturnArray = new gridReference[3];
        Random rand = new Random();
        int randomNum = rand.nextInt(101);
        if (focusArea.size() == 0) {
            // Needs to search for a new target
            tempReturnArray[0] = grdRefs[0][0];
            tempReturnArray[1] = grdRefs[0][1];
            tempReturnArray[2] = grdRefs[0][2];

            for (int y = 9; y > -1; y--) {
                for (int x = 0; x < 10; x++) {
                    // Loops through finding the highest weighting
                    try {
                        if (grdRefs[y][x].getWeight() >= tempReturnArray[0].getWeight()) {
                            if (grdRefs[y][x].getWeight() == tempReturnArray[0].getWeight()) {
                                // Loop finding the highest weight
                                if (randomNum > 50) {
                                    tempReturnArray[0] = grdRefs[y][x];
                                }
                            } else {
                                tempReturnArray[0] = grdRefs[y][x];
                            }
                        } else if ((grdRefs[y][x].getWeight() >= tempReturnArray[1].getWeight()) && (grdRefs[y][x].getWeight() < tempReturnArray[0].getWeight())) {
                            if (grdRefs[y][x].getWeight() == tempReturnArray[1].getWeight()) {
                                // Loop finding second highest weight
                                if (randomNum > 50) {
                                    tempReturnArray[1] = grdRefs[y][x];
                                }
                            } else {
                                tempReturnArray[1] = grdRefs[y][x];
                            }
                        } else if ((grdRefs[y][x].getWeight() >= tempReturnArray[2].getWeight()) && (grdRefs[y][x].getWeight() < tempReturnArray[1].getWeight())) {
                            if (grdRefs[y][x].getWeight() == tempReturnArray[2].getWeight()) {
                                // Loop finding third highest weight
                                if (randomNum > 50) {
                                    tempReturnArray[2] = grdRefs[y][x];
                                }
                            } else {
                                tempReturnArray[2] = grdRefs[y][x];
                            }
                        }
                    } catch (NullPointerException e) {
                        System.err.println("Finding weight rankings error");
                    }
                }
            }

        } else if (focusArea.size() == 1) {
            // Has hit one square
            gridReference[] storageArray = new gridReference[4]; // Stores 4 squares around the hit square
            storageArray[0] = new gridReference();
            storageArray[1] = new gridReference();
            storageArray[2] = new gridReference();
            storageArray[3] = new gridReference();
            try {
                storageArray[0] = grdRefs[focusArea.get(0).getGridLocation().width + 1][focusArea.get(0).getGridLocation().height];
            } catch (ArrayIndexOutOfBoundsException e) {
                // Doesn't assign a gridReference meaning returned weight will be 0
            }
            try {
                storageArray[1] = grdRefs[focusArea.get(0).getGridLocation().width - 1][focusArea.get(0).getGridLocation().height];
            } catch (ArrayIndexOutOfBoundsException e) {
                // Doesn't assign a gridReference meaning returned weight will be 0
            }
            try {
                storageArray[2] = grdRefs[focusArea.get(0).getGridLocation().width][focusArea.get(0).getGridLocation().height + 1];
            } catch (ArrayIndexOutOfBoundsException e) {
                // Doesn't assign a gridReference meaning returned weight will be 0
            }
            try {
                storageArray[3] = grdRefs[focusArea.get(0).getGridLocation().width][focusArea.get(0).getGridLocation().height - 1];
            } catch (ArrayIndexOutOfBoundsException e) {
                // Doesn't assign a gridReference meaning returned weight will be 0
            }
            tempReturnArray[0] = new gridReference();
            tempReturnArray[1] = new gridReference();
            tempReturnArray[2] = new gridReference();
            for (int x = 0; x < 4; x++) {
                if (storageArray[x].getWeight() >= tempReturnArray[0].getWeight()) {
                    if (storageArray[x].getWeight() == tempReturnArray[0].getWeight()) {
                        if (randomNum > 50) {
                            // Finding highest weighted side
                            tempReturnArray[0] = storageArray[x];
                        }
                    } else {
                        tempReturnArray[0] = storageArray[x];
                    }
                } else if ((storageArray[x].getWeight() >= tempReturnArray[1].getWeight()) && (storageArray[x].getWeight() < tempReturnArray[0].getWeight())) {
                    if (storageArray[x].getWeight() == tempReturnArray[1].getWeight()) {
                        // Finding second highest weighted side
                        if (randomNum > 50) {
                            tempReturnArray[1] = storageArray[x];
                        }
                    } else {
                        tempReturnArray[1] = storageArray[x];
                    }
                } else if ((storageArray[x].getWeight() >= tempReturnArray[2].getWeight()) && (storageArray[x].getWeight() < tempReturnArray[1].getWeight())) {
                    if (storageArray[x].getWeight() == tempReturnArray[2].getWeight()) {
                        // Finding third highest weighted side
                        if (randomNum > 50) {
                            tempReturnArray[2] = storageArray[x];
                        }
                    } else {
                        tempReturnArray[2] = storageArray[x];
                    }
                }
            }
        } else {
            // If more than one square have been hit
            if (focusArea.get(0).getGridLocation().width == focusArea.get(1).getGridLocation().width) {
                // Means the hits so far have been vertically aligned
                gridReference tempHighestPoint = focusArea.get(0);
                gridReference tempLowestPoint = focusArea.get(0);
                for (int x = 1; x < focusArea.size(); x++) {
                    if (focusArea.get(x).getGridLocation().height > tempHighestPoint.getGridLocation().height) {
                        // Loop finding which focusArea gridReference is the highest in the grid 
                        tempHighestPoint = focusArea.get(x);
                    } else if (focusArea.get(x).getGridLocation().height < tempLowestPoint.getGridLocation().height) {
                        // Loop finding which focusArea gridReference is the lowest in the grid 
                        tempLowestPoint = focusArea.get(x);
                    }
                }
                if ((tempHighestPoint.getGridLocation().height + 1 > 9) & (tempLowestPoint.getGridLocation().height - 1 < 0)) {
                    // Triggered if both the highest point is on the top row and the lowest point is on the lowest row
                    // Reset focus area and fire again
                    focusArea.clear();
                    findTargets();
                } else if (tempHighestPoint.getGridLocation().height + 1 > 9) {
                    // If highest point is on the top line of the grid sets the highest point to the lowest point to fire there instead
                    tempReturnArray[0] = grdRefs[focusArea.get(0).getGridLocation().width][tempLowestPoint.getGridLocation().height - 1];
                    tempReturnArray[1] = grdRefs[focusArea.get(0).getGridLocation().width][tempLowestPoint.getGridLocation().height - 1];
                    tempReturnArray[2] = grdRefs[focusArea.get(0).getGridLocation().width][tempLowestPoint.getGridLocation().height - 1];
                } else if (tempLowestPoint.getGridLocation().height - 1 < 0) {
                    // If lowest point is on the bottom line of the grid sets the lowest point to the highest point to fire there instead
                    tempReturnArray[0] = grdRefs[focusArea.get(0).getGridLocation().width][tempHighestPoint.getGridLocation().height + 1];
                    tempReturnArray[1] = grdRefs[focusArea.get(0).getGridLocation().width][tempHighestPoint.getGridLocation().height + 1];
                    tempReturnArray[2] = grdRefs[focusArea.get(0).getGridLocation().width][tempHighestPoint.getGridLocation().height + 1];
                } else {
                    // If neither the highest point is on the top line, or the lowest point is on the bottom line
                    if (grdRefs[focusArea.get(0).getGridLocation().width][tempHighestPoint.getGridLocation().height + 1].getWeight()
                            > grdRefs[focusArea.get(0).getGridLocation().width][tempLowestPoint.getGridLocation().height - 1].getWeight()) {
                        // If one above the highest point has the highest rating
                        tempReturnArray[0] = grdRefs[focusArea.get(0).getGridLocation().width][tempHighestPoint.getGridLocation().height + 1];
                        tempReturnArray[1] = grdRefs[focusArea.get(0).getGridLocation().width][tempLowestPoint.getGridLocation().height - 1];
                        tempReturnArray[2] = grdRefs[focusArea.get(0).getGridLocation().width][tempLowestPoint.getGridLocation().height - 1];
                    } else {
                        // If one below the lowest point has the highest rating
                        tempReturnArray[0] = grdRefs[focusArea.get(0).getGridLocation().width][tempLowestPoint.getGridLocation().height - 1];
                        tempReturnArray[1] = grdRefs[focusArea.get(0).getGridLocation().width][tempHighestPoint.getGridLocation().height + 1];
                        tempReturnArray[2] = grdRefs[focusArea.get(0).getGridLocation().width][tempHighestPoint.getGridLocation().height + 1];
                    }

                    if (tempReturnArray[0].getWeight() == 0) {
                        // If both above and below have a weight of 0, reset focusArea to find a new target.
                        focusArea.clear();
                        findTargets();
                    }
                }
            } else {
                // Means the hits so far have been horizontally aligned
                gridReference tempRightestPoint = focusArea.get(0);
                gridReference tempLeftestPoint = focusArea.get(0);
                for (int x = 1; x < focusArea.size(); x++) {
                    if (focusArea.get(x).getGridLocation().width > tempRightestPoint.getGridLocation().width) {
                        // Loop finding which focusArea gridReference is the rightest in the grid 
                        tempRightestPoint = focusArea.get(x);
                    } else if (focusArea.get(x).getGridLocation().width < tempLeftestPoint.getGridLocation().width) {
                        // Loop finding which focusArea gridReference is the leftest in the grid 
                        tempLeftestPoint = focusArea.get(x);
                    }
                }
                if ((tempRightestPoint.getGridLocation().width + 1 > 9) & (tempLeftestPoint.getGridLocation().width - 1 < 0)) {
                    // Triggered if both the rightest point is on the right column and the leftest point is on the leftest column
                    // Reset focus area and fire again
                    focusArea.clear();
                    findTargets();
                } else if (tempRightestPoint.getGridLocation().width + 1 > 9) {
                    // If rightest point is on the right column of the grid sets the point to fire at one past the leftest point
                    tempReturnArray[0] = grdRefs[tempLeftestPoint.getGridLocation().width - 1][focusArea.get(0).getGridLocation().width];
                    tempReturnArray[1] = grdRefs[tempLeftestPoint.getGridLocation().width - 1][focusArea.get(0).getGridLocation().width];
                    tempReturnArray[2] = grdRefs[tempLeftestPoint.getGridLocation().width - 1][focusArea.get(0).getGridLocation().width];
                } else if (tempLeftestPoint.getGridLocation().width - 1 < 0) {
                    // If leftest point is on the left column of the grid sets the point to fire at one past the rightest point
                    tempReturnArray[0] = grdRefs[tempRightestPoint.getGridLocation().width + 1][focusArea.get(0).getGridLocation().width];
                    tempReturnArray[1] = grdRefs[tempRightestPoint.getGridLocation().width + 1][focusArea.get(0).getGridLocation().width];
                    tempReturnArray[2] = grdRefs[tempRightestPoint.getGridLocation().width + 1][focusArea.get(0).getGridLocation().width];
                } else // If
                if (grdRefs[tempRightestPoint.getGridLocation().width + 1][focusArea.get(0).getGridLocation().height].getWeight()
                        > grdRefs[tempLeftestPoint.getGridLocation().width - 1][focusArea.get(0).getGridLocation().height].getWeight()) {

                    tempReturnArray[0] = grdRefs[tempRightestPoint.getGridLocation().width + 1][focusArea.get(0).getGridLocation().height];
                    tempReturnArray[1] = grdRefs[tempLeftestPoint.getGridLocation().width - 1][focusArea.get(0).getGridLocation().height];
                    tempReturnArray[2] = grdRefs[tempLeftestPoint.getGridLocation().width - 1][focusArea.get(0).getGridLocation().height];
                } else {
                    tempReturnArray[0] = grdRefs[tempLeftestPoint.getGridLocation().width - 1][focusArea.get(0).getGridLocation().height];
                    tempReturnArray[1] = grdRefs[tempRightestPoint.getGridLocation().width + 1][focusArea.get(0).getGridLocation().height];
                    tempReturnArray[2] = grdRefs[tempRightestPoint.getGridLocation().width + 1][focusArea.get(0).getGridLocation().height];
                }
            }

            if (tempReturnArray[0].getWeight() == 0) {
                // Triggered if all posible squares have a weight of 0
                focusArea.clear();
                return findTargets();
            } else if (tempReturnArray[1].getWeight() == 0) {
                // Triggered if second highest weight is 0
                tempReturnArray[1] = tempReturnArray[0];
                tempReturnArray[2] = tempReturnArray[0];
            } else if (tempReturnArray[2].getWeight() == 0) {
                // Triggered if third highest weight is 0
                tempReturnArray[2] = tempReturnArray[1];
            }

        }

        return tempReturnArray;
    }

    private void weightCalc(gridReference[] LineToTest) {
        for (int x = 0; x < LineToTest.length; x++) {
            try {
                if (LineToTest[x].getMiss() == false) {
                    // 2 Ship Controller
                    if (x > 0) {
                        if (ships[0].getSunk() == false) {
                            LineToTest[x - 1].addWeight(1);
                            LineToTest[x].addWeight(1);
                        }
                    } else {
                        continue;
                    }

                    // 3 Ship Controller
                    if (x > 1) {
                        if (ships[1].getSunk() == false) {
                            LineToTest[x - 2].addWeight(1);
                            LineToTest[x - 1].addWeight(1);
                            LineToTest[x].addWeight(1);
                        }
                    } else {
                        continue;
                    }

                    // 3 Ship Controller
                    if (x > 1) {
                        if (ships[2].getSunk() == false) {
                            LineToTest[x - 2].addWeight(1);
                            LineToTest[x - 1].addWeight(1);
                            LineToTest[x].addWeight(1);
                        }
                    } else {
                        continue;
                    }

                    // 4 Ship Controller
                    if (x > 2) {
                        if (ships[3].getSunk() == false) {
                            LineToTest[x - 3].addWeight(1);
                            LineToTest[x - 2].addWeight(1);
                            LineToTest[x - 1].addWeight(1);
                            LineToTest[x].addWeight(1);
                        }
                    } else {
                        continue;
                    }

                    // 5 Ship Controller
                    if (x > 3) {
                        if (ships[4].getSunk() == false) {
                            LineToTest[x - 4].addWeight(1);
                            LineToTest[x - 3].addWeight(1);
                            LineToTest[x - 2].addWeight(1);
                            LineToTest[x - 1].addWeight(1);
                            LineToTest[x].addWeight(1);
                        }
                    } else {
                        continue;
                    }

                } else {
                    gridReference[] nextLineToTest = new gridReference[(LineToTest.length - 1) - x];
                    for (int y = 0; y < (LineToTest.length - x - 1); y++) {
                        nextLineToTest[y] = LineToTest[y + x + 1];
                    }
                    weightCalc(nextLineToTest);
                    break;
                }
                setGridColors();

            } catch (ArrayIndexOutOfBoundsException e) {
                System.err.println("ArrayOutOfBounds");
                break;
            }
        }

    }

    private void resetGridWeight() {
        try {
            for (int x = 9; x > -1; x--) {
                for (int y = 0; y < 10; y++) {
                    grdRefs[y][x].resetWeight();
                }
            }
        } catch (NullPointerException e) {
            System.err.println("setResetGridWeight Err");
        }
    }

    private void initShips() {
        try {
            ships = new ship[]{new ship(new gridReference[]{new gridReference(), new gridReference()}),
                new ship(new gridReference[]{new gridReference(), new gridReference(), new gridReference()}),
                new ship(new gridReference[]{new gridReference(), new gridReference(), new gridReference()}),
                new ship(new gridReference[]{new gridReference(), new gridReference(), new gridReference(), new gridReference()}),
                new ship(new gridReference[]{new gridReference(), new gridReference(), new gridReference(), new gridReference(), new gridReference()})};
            ships[0].setName("Destroyer");
            ships[1].setName("Submarine");
            ships[2].setName("Cruiser");
            ships[3].setName("Battleship");
            ships[4].setName("Carrier");

        } catch (NullPointerException e) {
            System.err.println("InitShips Err");
        }
    }

    public void setEnemyMode(boolean newEnemyMode) {
        try {
            for (int x = 9; x > -1; x--) {
                for (int y = 0; y < 10; y++) {
                    grdRefs[y][x].setEnemyMode(newEnemyMode);
                }
            }
        } catch (NullPointerException e) {
            System.err.println("setEnemyMode Err");
        }
    }

    public void placeShip(int shipID, Dimension startPos, int orientation) {
        try {
            gridReference[] oldContainer = ships[shipID].getContainer();
            for (int shipSlot = 0; shipSlot < oldContainer.length; shipSlot++) {
                if (oldContainer[shipSlot].getConflicts() > 0) {
                    oldContainer[shipSlot].modConflicts(-1);
                }
            }

            gridReference[] newContainer = new gridReference[ships[shipID].getLength()];
            int addWidth = 0;
            int addHeight = 0;
            for (int shipSlot = 0; shipSlot < ships[shipID].getLength(); shipSlot++) {
                if (orientation == 0) {
                    addWidth = shipSlot;
                } else {
                    addHeight = shipSlot;
                }

                for (int shipCheck = 0; (shipCheck < 5); shipCheck++) {
                    if (shipCheck != shipID) {
                        if (ships[shipCheck].doesContain(grdRefs[startPos.width + addWidth][startPos.height - addHeight])) {
                            grdRefs[startPos.width + addWidth][startPos.height - addHeight].modConflicts(1);
                            break;
                        }
                    }
                }

                newContainer[shipSlot] = grdRefs[startPos.width + addWidth][startPos.height - addHeight];
            }

            ships[shipID].setContainer(newContainer);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("Array placeship err");

        } catch (NullPointerException e) {
            System.err.println("Null placeship err");

        }
        sortShipOccupation();
        setGridColors();

    }

    public Dimension getShipLocation(int ShipID) {
        return ships[ShipID].getStartPos();
    }

    public int getShipOrient(int ShipID) {
        return ships[ShipID].getOrientation();
    }

    public void setGridColors() {
        try {
            for (int x = 9; x > -1; x--) {
                for (int y = 0; y < 10; y++) {
                    grdRefs[y][x].setColors();
                }
            }
        } catch (NullPointerException e) {
            System.err.println("GridColors Err");
        }
    }

    public void setDifficulty(int newDifficulty) {
        if ((0 < newDifficulty) & (4 > newDifficulty)) {
            difficulty = newDifficulty;
        }
    }

    public ship[] getShips() {
        return ships;
    }

    public boolean gridHasConflicts() {
        for (int x = 9; x > -1; x--) {
            for (int y = 0; y < 10; y++) {
                if (grdRefs[y][x].getConflicts() > 0) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean allShipsSunk() {
        for (int shipID = 0; shipID < 5; shipID++) {
            if (ships[shipID].getSunk() == false) {
                return false;
            }
        }
        return true;
    }

    private void sortShipOccupation() {
        for (int x = 0; x < 5; x++) {
            ships[x].setContainer(ships[x].getContainer());
        }
    }
}
